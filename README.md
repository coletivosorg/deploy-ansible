
# Usage:

Try something cooler:
Ping all servers
  `$ ./coletivos.sh ping`
Play a playbook (check and set)
  `$ ./coletivos.sh play (playbook)`
Manage ur instances
  `$ ./coletivos.sh instances [up|setup|down|upgrade|restart] (instance_name)`
List all possible playbooks
  `$ ls tasks/basics/ `

## ping all servers
./coletivos.sh ping

## bootstrap a server
./coletivos.sh play bootstrap

## secure a server
./coletivos.sh play secure

## create users and add publickey
./coletivos.sh play users

## reset a user password
./coletivos.sh play resetuser


### Instances:

./coletivos.sh instances up

./coletivos.sh instances setup wptest

./coletivos.sh instances setup

./coletivos.sh instances up wptest