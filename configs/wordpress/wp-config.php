<?php
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) $_SERVER['HTTPS']='on';

define( 'DB_NAME', '{{ instance_config.key }}' );
define( 'DB_USER', '{{ instance_config.key }}_usr' );
define( 'DB_PASSWORD', '{{ instance_config.value.db.password }}' );
define( 'DB_HOST', 'mysql' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
define( 'AUTH_KEY',         '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'SECURE_AUTH_KEY',  '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'LOGGED_IN_KEY',    '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'NONCE_KEY',        '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'AUTH_SALT',        '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'SECURE_AUTH_SALT', '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'LOGGED_IN_SALT',   '{{ 999999999999999999994 | random | to_uuid }}' );
define( 'NONCE_SALT',       '{{ 999999999999999999994 | random | to_uuid }}' );
$table_prefix = 'wp_';
define( 'WP_DEBUG', false );

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
require_once( ABSPATH . 'wp-settings.php' );
