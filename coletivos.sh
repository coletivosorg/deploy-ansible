#!/bin/bash

HOSTSFILE="defs/ansible/hosts"
export ANSIBLE_CONFIG="defs/ansible/ansible.cfg"

if [ -z "$1" ]
then
    echo "Try something cooler:"
    echo "Ping all servers"
    echo "  $ ./coletivos.sh ping"
    echo "Play a playbook (check and set)"
    echo "  $ ./coletivos.sh play (playbook)"
    echo "Manage ur instances"
    echo "  $ ./coletivos.sh instances [up|setup|down|upgrade|restart] (instance_name)"
    echo "List all possible playbooks"
    echo "  $ ls tasks/basics/" 
else
    
    if [ $1 == "ping" ]
    then
        ansible all -m ping -i $HOSTSFILE
    fi
    
    if [ $1 == "play" ]
    then
        ansible-playbook tasks/basics/$2.yaml -i $HOSTSFILE -vv --ask-sudo-pass
    fi
    
    if [ $1 == "instances" ]
    then
        if [ $2 == "up" ]
        then    
        ansible-playbook tasks/instances/up.yaml --extra-vars "instance=$3" -i $HOSTSFILE
    fi
    if [ $2 == "setup" ]
    then    
    ansible-playbook tasks/instances/setup.yaml --extra-vars "instance=$3" -i $HOSTSFILE --ask-sudo-pass
fi

# if [ $1 == "down" ]
#   then
#       ansible-playbook tasks/basics/apps.yaml -i $HOSTSFILE -vv --ask-sudo-pass
# fi
# 
# if [ $1 == "upgrade" ]
#   then
#       ansible-playbook tasks/basics/$2.yaml -i $HOSTSFILE -vv --ask-sudo-pass
# fi
fi
fi